# Excel Access via Java C Sharp Python Demo

This demo contains basic code to access Excel sheet data via the following programming languages:
- Java
- C#
- Python

<br />
Usage requirements:<br />

Java:<br />
1. Downloaded Apache POI Libraries (https://poi.apache.org/)<br />
	a) Add necessary .xlsx POI libraries as external libraries in your Java IDE<br />
	b) Add necessary .xlsx POI libraries to your Java classpath and run the Main.java

C#:<br />
1. PIA (Primary Interop Assemblies)<br />
	a) Installed Office version (includes installed Primary Interop Assemblies) <br />
	b) Installed Primary Interop Assemblies

Python:<br />
1. Installed OpenPyXL Library (https://openpyxl.readthedocs.io/en/stable/#)

!Only tested against the following Environment!
- Win10
- IntelliJ (Java) with Apache POI v5.0.0
- Visual Studio 2019 (C#) with installed Office 2016 => Microsoft Excel 16.0 Object Library
- Visual Studio Code (Python 2.7) with installed OpenPyXL